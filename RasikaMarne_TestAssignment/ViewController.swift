//
//  ViewController.swift
//  RasikaMarne_TestAssignment
//
//  Created by Rasika Marne on 11/6/17.
//  Copyright © 2017 Rasika Marne. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var weatherInfoTableView: UITableView!
    var weatherModObj:WeatherModel = WeatherModel()
    var weatherArray:[WeatherModel] = [WeatherModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let cityIds:String = "\(SYDNEY_ID),\(MELBOURNE_ID),\(BRISBANE_ID)"
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.getWeatherOfCity(cityIds: cityIds)
        //self.getWeatherOfCity(cityName: "Melbourne")
        //self.getWeatherOfCity(cityName: "Brisbane")
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    func getWeatherOfCity(cityIds:String){
       // var cityId:String?
       // for city in cityNameArr{
            APIManager.sharedInstance.getWeatherOfCity(cityName: cityIds, success: { (weatherModArr) in
               // self.weatherArray.append(weatherMod)
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                if weatherModArr.count > 0{
                    self.weatherArray = (weatherModArr as? [WeatherModel])!
                    self.weatherInfoTableView.reloadData()
                }
                
            }, failure: { (error) in
                print("error")
            })
       // }
        print(self.weatherArray.count)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "detailViewId"{
            let vc = segue.destination as! DetailsViewController
            vc.selectedCity = self.weatherModObj
            //vc.backImageName = backImageName
            //vc.selectedModule = selectedModule
            
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.weatherArray.count > 0 {
            return self.weatherArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79.0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier")! as UITableViewCell
        let modObject:WeatherModel = self.weatherArray[indexPath.row]
        let cityNameLbl = cell.viewWithTag(1) as! UILabel
        let tempLbl = cell.viewWithTag(2) as! UILabel
        cityNameLbl.text = modObject.name
        tempLbl.text = "\(modObject.temp!)∘C"
        cell.selectionStyle = .none
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if selectedModule == "MEETINGS SCHEDULED BY ME" || selectedModule == "MEETING INVITATIONS" {
//            selectedMeetingObj = scheduledByMeArray[indexPath.row] as! MeetingModel
//            performSegue(withIdentifier: "MeetingDetail", sender: self)
//        }
        self.weatherModObj = weatherArray[indexPath.row]
        performSegue(withIdentifier: "detailViewId", sender: self)
    }

}

