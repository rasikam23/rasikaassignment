//
//  WeatherModel.swift
//  RasikaMarne_TestAssignment
//
//  Created by Rasika Marne on 11/6/17.
//  Copyright © 2017 Rasika Marne. All rights reserved.
//

import UIKit

class WeatherModel: NSObject {

    var name:String?
    var id:String?
    var temp:String?
    var temp_max:String?
    var temp_min:String?
    var desc:String?
    var humidity:String?
    var sunRiseTime:String?
    var sunSetTime:String?
}
