//
//  DetailsViewController.swift
//  RasikaMarne_TestAssignment
//
//  Created by Rasika Marne on 11/6/17.
//  Copyright © 2017 Rasika Marne. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblSunsetTime: UILabel!
    @IBOutlet weak var lblSunrisetime: UILabel!
    @IBOutlet weak var lblHumdity: UILabel!
    @IBOutlet weak var lblMaximumTemp: UILabel!
    @IBOutlet weak var lblMinimumTemp: UILabel!
    var selectedCity:WeatherModel = WeatherModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblDesc.text = selectedCity.desc
        lblHumdity.text = selectedCity.humidity
        lblMinimumTemp.text = "\(selectedCity.temp_min!)∘C"
        lblMaximumTemp.text = "\(selectedCity.temp_max!)∘C"
        lblSunsetTime.text = selectedCity.sunSetTime
        lblSunrisetime.text = selectedCity.sunRiseTime
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
