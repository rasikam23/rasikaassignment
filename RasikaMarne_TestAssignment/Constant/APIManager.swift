//
//  APIManager.swift
//  RasikaMarne_TestAssignment
//
//  Created by Rasika Marne on 11/7/17.
//  Copyright © 2017 Rasika Marne. All rights reserved.
//

import Foundation

class APIManager: NSObject {

    static let sharedInstance = APIManager()

    func getWeatherOfCity(cityName:String,success: @escaping ( _ response: NSArray) -> Void, failure: @escaping ( _ error: NSError?) -> Void){
    var cityId:String?
   // for city in cityNameArr{
//        switch cityName {
//        case "Sydney":
//            cityId = SYDNEY_ID
//            break
//        case "Melbourne":
//            cityId = MELBOURNE_ID
//            break
//        case "Brisbane":
//            cityId = BRISBANE_ID
//            break
//        default:
//            break
//        }
        
        let urlStr  = BASE_URL+"?id=\(cityName)&units=metric&appid=\(API_KEY)"
        print(urlStr)
        let url:NSURL = NSURL(string: urlStr)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                print("error")
                return
            }
            do {
                let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                print(dict ?? "")
                Parser.sharedInstance.parseResponse(dict: dict! as NSDictionary, success: { (weatherMod) in
                   // self.weatherArray.append(weatherMod)
                    success(weatherMod)
                }, failure: { (err) in
                    failure(err as NSError)
                })
               // self.weatherModObj = self.parseResponse(dict: (dict as NSDictionary?)!, cityName: city)
              //  self.weatherArray.append(self.weatherModObj)
               // print(self.weatherArray)
            } catch {
                print(error.localizedDescription)
            }
            
           
        }
        task.resume()
    }
}
