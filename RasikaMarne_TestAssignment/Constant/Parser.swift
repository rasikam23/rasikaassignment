//
//  Parser.swift
//  RasikaMarne_TestAssignment
//
//  Created by Rasika Marne on 11/7/17.
//  Copyright © 2017 Rasika Marne. All rights reserved.
//

import Foundation

class Parser: NSObject {
    
    static let sharedInstance = Parser()
    func parseResponse(dict:NSDictionary,success: (  _ weatherModArr : NSArray) -> Void, failure : ( _ error : Error) -> Void){
    var descStr:String = ""
        var modArray = [WeatherModel]()
    
        let listArr = dict.value(forKey: "list") as? NSArray
        for dict1 in listArr!{
            let modObject:WeatherModel = WeatherModel()
            let dict2 = dict1 as? NSDictionary
            modObject.id = dict2?.value(forKey: "id") as? String
            modObject.name = dict2?.value(forKey: "name") as? String
            let mainDict = dict2?.value(forKey: "main") as? NSDictionary
            let humidity:Int = (mainDict?.value(forKey: "humidity") as? Int)!
            let temp:Double = (mainDict?.value(forKey: "temp") as? Double)!
            let temp_max:Int = (mainDict?.value(forKey: "temp_max") as? Int)!
            let temp_min:Int = (mainDict?.value(forKey: "temp_min") as? Int)!
            modObject.humidity = "\(humidity)"
            modObject.temp = "\(temp)"
            modObject.temp_max = "\(temp_max)"
            modObject.temp_min = "\(temp_min)"
            let weatherArr = dict2?.value(forKey: "weather") as? NSArray
            for obj in weatherArr!{
                let dict3 = obj as? NSDictionary
                let descStr2 = dict3?.value(forKey: "description") as? String
                descStr = descStr + descStr2! + "\n"
                modObject.desc = descStr
                
            }
            let sysDict = dict2?.value(forKey: "sys") as? NSDictionary
            let riseDate = NSDate(timeIntervalSince1970: sysDict?.value(forKey: "sunrise") as! TimeInterval)
            //NSDate.init(timeIntervalSinceReferenceDate: sysDict?.value(forKey: "sunrise") as! TimeInterval)
            let setDate = NSDate(timeIntervalSince1970: sysDict?.value(forKey: "sunset") as! TimeInterval)//NSDate.init(timeIntervalSinceReferenceDate: sysDict?.value(forKey: "sunset") as! TimeInterval)
            let dateFormat:DateFormatter = DateFormatter()
          //  dateFormat.timeZone = TimeZone(abbreviation: "UTC+10:00")
            dateFormat.timeZone = TimeZone(abbreviation: "AEST")
            dateFormat.dateFormat = "MMM, dd hh:mm a"
            let riseDateStr = dateFormat.string(from: riseDate as Date)
            let setDateStr = dateFormat.string(from: setDate as Date)
            modObject.sunSetTime = setDateStr
            modObject.sunRiseTime = riseDateStr
            modArray.append(modObject)
        }
    
    //return modObject
    success(modArray as NSArray)
    }
}
